#!/bin/sh

set -eu

BORGMATIC_VERSION=$(borgmatic --version)

curl --silent -J -o "borgmatic-docs-${BORGMATIC_VERSION}.tar.gz" "https://projects.torsion.org/api/packages/borgmatic-collective/generic/borgmatic-docs/${BORGMATIC_VERSION}/borgmatic-docs.tar.gz"

echo "borgmatic-docs-${BORGMATIC_VERSION}.tar.gz"
